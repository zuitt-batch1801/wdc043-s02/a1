import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class Main {
    public static void main (String[] args) {

        // ARRAYS

        String[] fruits = new String[5];
        fruits[0] = "apple";
        fruits[1] = "avocado";
        fruits[2] = "banana";
        fruits[3] = "kiwi";
        fruits[4] = "orange";

        System.out.println("Fruits in stock:" + Arrays.toString(fruits));

        Scanner scannerName = new Scanner(System.in);


        System.out.println("What fruit would you like to get the index of?");
        String indexFruit = scannerName.nextLine();
        int result = Arrays.binarySearch(fruits, indexFruit);
        System.out.println("The index of " + indexFruit + " is: " + result);


        // ARRAYLIST

        ArrayList<String> friends = new ArrayList<>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);


        // HASHMAP

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);









    }
}
